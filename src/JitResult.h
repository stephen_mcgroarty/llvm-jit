#ifndef JIT_RESULT_H
#define JIT_RESULT_H
#include "llvm/IR/Module.h"
#include "llvm/ExecutionEngine/MCJIT.h"


class JitResult
{
public:
	JitResult(llvm::ExecutionEngine * engine): m_pEngine(engine) {}

	template<typename FunctionType> 
	FunctionType GetFunction(const std::string & functionName);


	void * GetGlobalAddr(const std::string & var);
private:
	// This memory is managed elsewhere because LLVM.
	llvm::ExecutionEngine * m_pEngine;
};


template<typename FunctionType>
FunctionType JitResult::GetFunction(const std::string & functionName)
{
	FunctionType function = reinterpret_cast<FunctionType>
											(m_pEngine->getFunctionAddress(functionName));
	if (!function)
	{
		printf("Error: Could not find function %s in module\n", functionName.c_str());
		return 0;
	}
	return function;
}

#endif