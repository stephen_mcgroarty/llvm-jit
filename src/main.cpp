#include "Jitter.h"


int main(int argc,char **argv)
{
	std::string filename {"test.c"};
	std::string functionName {"func"};

	std::string code = {"int g_X = 42; int func(int x){return x/2 + g_X;}"};
	Jitter jitter;

	std::shared_ptr<JitResult> result = jitter.JitCode(code);
	Jitter::JittedFunctionType func = result->GetFunction<Jitter::JittedFunctionType>(functionName);

	if (func)
	{
		printf("String function returned value %d\n",func(1024));
	}

	void * global = result->GetGlobalAddr("g_X");

	if (global)
	{
		printf("Global g_X has value: %d\n", *(int*)global);
		*(int*)global = 20;
		printf("Global g_X has value: %d\n", *(int*)global);
		printf("String function now returns value %d\n",func(1024));

	}


	std::shared_ptr<JitResult> fileJitResult = jitter.JitFile(filename);
	Jitter::JittedFunctionType func2 = fileJitResult->GetFunction<Jitter::JittedFunctionType>(functionName);

	if (func2)
	{
		printf("File function returned value %d\n",func2(1024));
	}

	return 0;
}