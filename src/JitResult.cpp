#include "JitResult.h"



void * JitResult::GetGlobalAddr(const std::string & varName)
{
	void * global = reinterpret_cast<void *>(m_pEngine->getGlobalValueAddress(varName));

	if (!global)
	{
		fprintf(stderr, "Could not find global wiht name %s\n",varName.c_str());
	}

	return global;
}
