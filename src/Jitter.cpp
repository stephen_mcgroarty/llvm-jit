#include "Jitter.h"
#include <stdio.h>
#include "llvm/ADT/STLExtras.h"
#include "llvm/ExecutionEngine/GenericValue.h"
#include "llvm/ExecutionEngine/Interpreter.h"
#include "llvm/IR/Constants.h"
#include "llvm/IR/DerivedTypes.h"
#include "llvm/IR/IRBuilder.h"
#include "llvm/IR/Instructions.h"
#include "llvm/Support/ManagedStatic.h"
#include "llvm/Support/TargetSelect.h"
#include "llvm/Support/raw_ostream.h"

#include "clang/Basic/DiagnosticOptions.h"
#include "clang/Frontend/TextDiagnosticPrinter.h"
#include "clang/Basic/LangOptions.h"
#include "clang/Basic/FileSystemOptions.h"
#include "clang/Basic/SourceManager.h"
#include "clang/Lex/HeaderSearch.h"
#include "clang/Basic/FileManager.h"
#include "clang/Frontend/Utils.h"
#include "clang/Basic/TargetOptions.h"
#include "clang/Basic/TargetInfo.h"
#include "clang/Lex/Preprocessor.h"
#include "clang/Frontend/FrontendOptions.h"
#include "clang/Basic/IdentifierTable.h"
#include "clang/Basic/Builtins.h"
#include "clang/AST/ASTContext.h"
#include "clang/AST/ASTConsumer.h"
#include "clang/CodeGen/CodeGenAction.h"
#include "clang/Parse/ParseAST.h"
#include "clang/Parse/Parser.h"
#include "clang/Frontend/CompilerInstance.h"
#include <vector>
#include <time.h>
#include <cmath>
#include <stdio.h>

Jitter::Jitter():m_Warnings(false), m_pRuntime(nullptr)
{
	// Needed for MCJit
	llvm::InitializeNativeTarget();
	llvm::InitializeNativeTargetAsmPrinter();

	CompileRuntime();
}


Jitter::~Jitter()
{
	for (auto & file : m_FileCache)
	{
		remove(file.c_str());
	}
}


void Jitter::CompileRuntime()
{
	std::string code {"int Add(int x){return x/2;}"};
	std::string filename {"runtime.c"};
	SaveCodeToFile(code,filename);

	std::unique_ptr<clang::CodeGenAction> codeGen = Jitter::RunClang(filename);
	m_pRuntime = codeGen->takeModule();
	m_CompiledFiles.push_back(std::move(codeGen));
}



void Jitter::SaveCodeToFile(const std::string & source,const std::string &filename)
{
	std::string tempFilename;
	if (filename == "")
	{
		tempFilename = GenerateFileName();
	} else
	{
		tempFilename = filename;
	}


	FILE * file = fopen(tempFilename.c_str(),"wb");
	fwrite(source.c_str(),1,source.size(),file);
	fclose(file);	
	m_FileCache.push_back(tempFilename);

}




std::shared_ptr<JitResult> Jitter::JitCode(const std::string & source, bool link)
{
	std::string filename = GenerateFileName();
	FILE * file = fopen(filename.c_str(),"wb");
	fwrite(source.c_str(),1,source.size(),file);
	fclose(file);

	m_FileCache.push_back(filename);

	return JitFile(filename,link);
}


std::string Jitter::GenerateFileName() const
{
	srand(time(nullptr));
	const int NAME_LENGTH = 64;
	std::string filename;

	for(int i = 0; i < NAME_LENGTH; ++i)
	{
		char str[2] = {static_cast<char>('a' + (rand()%26)), '\0'};

		filename += std::string(str);
	}
	filename += std::string(".c");
	return filename;
}


std::unique_ptr<clang::CodeGenAction> Jitter::RunClang(const std::string & filename) const
{
// Compilation arguments to pass to clang
	std::vector<const char *> args;
	args.push_back(filename.c_str());

	if (!m_Warnings)
	{
		args.push_back("-w");
	}

	llvm::IntrusiveRefCntPtr<clang::DiagnosticOptions> DiagOpts =new clang::DiagnosticOptions();
	llvm::IntrusiveRefCntPtr<clang::DiagnosticIDs> DiagID(new clang::DiagnosticIDs());

	clang::DiagnosticsEngine Diags(DiagID, &*DiagOpts);
	
	// Ownership of this pointer is passed to the compiler invocation.
	clang::CompilerInvocation *CI {new clang::CompilerInvocation};
	clang::CompilerInvocation::CreateFromArgs(*CI, &args[0], &args[0] + args.size(), Diags);

	clang::CompilerInstance Clang;
	Clang.setInvocation(CI);

	Clang.createDiagnostics();
	if (!Clang.hasDiagnostics())
	{
		printf("Error: Could create diagnostics for clang for file %s",filename.c_str());
		return nullptr;
	}

	std::unique_ptr<clang::CodeGenAction> action(new clang::EmitLLVMOnlyAction());
	if (!Clang.ExecuteAction(*action))
	{
		printf("Error: Could create generate code for file %s",filename.c_str());
		return nullptr;
	}

	return action;
}


std::shared_ptr<JitResult> Jitter::JitFile(const std::string & filename,bool link)
{
	auto codeGen = RunClang(filename);
	std::unique_ptr<llvm::Module> module = codeGen->takeModule();

	m_CompiledFiles.push_back(std::move(codeGen));

	llvm::EngineBuilder engBuilder(std::move(module));
	engBuilder.setEngineKind(llvm::EngineKind::JIT);
	engBuilder.setOptLevel(llvm::CodeGenOpt::Aggressive);
	engBuilder.setMArch("x86-64");


	llvm::ExecutionEngine *engine = engBuilder.create();

	if (link)
	{
		engine->generateCodeForModule(m_pRuntime.get());
	}

	engine->finalizeObject();

	std::shared_ptr<JitResult> result {new JitResult(engine)};
	return result;
}
