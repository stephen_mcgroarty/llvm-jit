#ifndef JITTER_H
#define JITTER_H
#include "llvm/IR/LLVMContext.h"
#include "clang/CodeGen/CodeGenAction.h"
#include <memory>
#include <string>
#include <vector>
#include <map>
#include "JitResult.h"

class Jitter 
{
public:
	typedef int (*JittedFunctionType)(int);

	Jitter();
	~Jitter();

	std::shared_ptr<JitResult> JitCode(const std::string & source, bool link = true);
	std::shared_ptr<JitResult> JitFile(const std::string & filename, bool link = true);


	void SetWarnings(bool warnings) { m_Warnings = warnings; }

	std::unique_ptr<clang::CodeGenAction> RunClang(const std::string & filename) const;
private:
	llvm::LLVMContext m_Context;
	std::string GenerateFileName() const;

	bool m_Warnings;
	void SaveCodeToFile(const std::string & source,const std::string &filename = "");
	std::vector<std::string> m_FileCache;



	void CompileRuntime();

	// These will fall out of scope if 
	std::vector<std::unique_ptr<clang::CodeGenAction>> m_CompiledFiles;
	std::unique_ptr<llvm::Module> m_pRuntime;
};


#endif