set(CLANG_INCLUDE_DIRS "${LLVM_SRC_DIR}/tools/clang/include/" "${LLVM_BUILD_DIR}/tools/clang/include")
find_library(CLANG_LIBRARIES NAMES clang HINTS "${LLVM_BUILD_DIR}/lib")


set(CLANG_LINKER_FLAGS "-lclangTooling -lclangFrontendTool -lclangFrontend -lclangDriver -lclangSerialization -lclangCodeGen -lclangParse -lclangSema -lclangStaticAnalyzerFrontend -lclangStaticAnalyzerCheckers -lclangStaticAnalyzerCore -lclangAnalysis -lclangARCMigrate -lclangRewrite -lclangRewriteFrontend -lclangEdit -lclangAST -lclangLex -lclangBasic")

include(FindPackageHandleStandardArgs)

message(STATUS "Clang libraries: ${CLANG_LIBRARIES}")
message(STATUS "Clang headers: ${CLANG_INCLUDE_DIRS}")

find_package_handle_standard_args(Clang DEFAULT_MSG CLANG_LIBRARIES CLANG_INCLUDE_DIRS)